package com.mycompany.app;

import java.util.Arrays;

public class App {
    public static void main(String[] args) {
        if (args.length > 0) {
            int[] numbers = new int[args.length];
            for (int i = 0; i < args.length; i++) {
                try {
                    numbers[i] = Integer.parseInt(args[i]);
                } catch (NumberFormatException e) {
                    System.err.println("Invalid argument: " + args[i]);
                    return;
                }
            }
            numbers = sort(numbers);
            System.out.println("Sorted numbers:");
            for (int num : numbers) {
                System.out.println(num);
            }
        } else {
            System.out.println("No arguments provided.");
        }
    }

    public static int[] sort(int[] arr) {
        Arrays.sort(arr);
        return arr;
    }
}
