package com.mycompany.app;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class AppTest {

    private final int[] input;
    private final int[] expected;

    public AppTest(int[] input, int[] expected) {
        this.input = input;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new int[]{}, new int[]{}},  // Test empty array
                {new int[]{5, 3, 8, 1, 9}, new int[]{1, 3, 5, 8, 9}},  // Test random numbers
                {new int[]{5, 5, 5, 5, 5}, new int[]{5, 5, 5, 5, 5}},  // Test equal numbers
                {new int[]{1, 2, 3, 4, 5}, new int[]{1, 2, 3, 4, 5}},  // Test already sorted array
                {new int[]{9, 8, 7, 6, 5}, new int[]{5, 6, 7, 8, 9}},  // Test reverse sorted array
        });
    }

    @Test
    public void testSortApp() {
        assertArrayEquals(expected, App.sort(input));
    }
}

